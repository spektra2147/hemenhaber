<?php namespace Visiosoft\MetsaTheme\Http\Controller;

use Anomaly\Streams\Platform\Http\Controller\PublicController;
use Anomaly\Streams\Platform\Model\Posts\PostsDefaultPostsEntryModel;

class PostsController extends PublicController
{

    public function view_counter(PostsDefaultPostsEntryModel $postModel, $post_id)
    {
        $post = $postModel->where('id', $post_id);
        $post_detail = $post->first();
        $post->update(['view_count' => $post_detail->view_count+1]);
    }

}