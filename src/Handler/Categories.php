<?php namespace Visiosoft\MetsaTheme\Handler;

use Anomaly\CheckboxesFieldType\CheckboxesFieldType;
use Anomaly\PostsModule\Category\Contract\CategoryRepositoryInterface;

class Categories
{

    public function handle(CheckboxesFieldType $fieldType, CategoryRepositoryInterface $category)
    {
            $fieldType->setOptions(
                $category->all()->pluck(
                    'name','id'
                )->all());
    }
}
