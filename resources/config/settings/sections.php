<?php

return [
    'monitoring' => [
        'stacked' => true,
        'tabs'    => [
            'logo'      => [
                'title'  => 'Logo Ayarları',
                'fields' => [
                    'web-logo','mobile-logo',
                ],
            ],
            'footer'      => [
                'title'  => 'Footer Ayarları',
                'fields' => [
                    'site-description','site-address','site-support-phone','twitter-feed'
                ],
            ],
            'banner'      => [
                'title'  => 'Banner Ayarları',
                'fields' => [
                    'header-banner-type','header-banner-image','header-banner-code',
                    'content-banner-type','content-banner-image','content-banner-code',
                ],
            ],
            'sidebar'      => [
                'title'  => 'Sidebar Banner Ayarları',
                'fields' => [
                    'home-sidebar-banner-one-type','home-sidebar-banner-one-image','home-sidebar-banner-one-code',
                    'detail-sidebar-banner-one-type','detail-sidebar-banner-one-image','detail-sidebar-banner-one-code',
                    'detail-sidebar-banner-two-type','detail-sidebar-banner-two-image','detail-sidebar-banner-two-code',
                    'categories-sidebar-banner-one-type','categories-sidebar-banner-one-image','categories-sidebar-banner-one-code',
                    'categories-sidebar-banner-two-type','categories-sidebar-banner-two-image','categories-sidebar-banner-two-code',
                ],
            ],
            'headline'      => [
                'title'  => 'Manşet Ayarları',
                'fields' => [
                    'headline-center-type','headline-top-left','headline-bottom-left','headline-top-right','headline-bottom-right'
                ],
            ],
            'breaking'      => [
                'title'  => 'Sondakika Şerit Ayarları',
                'fields' => [
                    'breaking-news-type'
                ],
            ],
            'content'      => [
                'title'  => 'Anasayfa İçerik Ayarları',
                'fields' => [
                    'popular-news-slider','featured-category-section','bottom-section-one-column-one-name',
                    'bottom-section-one-column-one','bottom-section-two-column-name','bottom-section-two-categories',
                    'youtube-api-key','youtube-playlist-code','footer-news-block-one',
                    'footer-news-block-two','footer-news-block-three'
                ],
            ],
            'social'      => [
                'title'  => 'Sosyal Paylaşım Ayarları',
                'fields' => [
                    'facebook-url','twitter-url',
                    'instagram-url','youtube-url',
                ],
            ],
            'navigation'      => [
                'title'  => 'Menü Ayarları',
                'fields' => [
                    'navigation-categories',
                ],
            ],
        ],
    ],
];
